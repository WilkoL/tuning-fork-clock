#include "tm1637_minimum.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>


//private functions
void TM1637_WriteByte(uint8_t);
void TM1637_start(void);
void TM1637_stop(void);
uint8_t TM1637_decode(uint8_t, uint8_t);

static int8_t decode_table[10] =
{
	0x3f,0x06,0x5b,0x4f,			//0,1,2,3,
	0x66,0x6d,0x7d,0x07,			//4,5,6,7,
	0x7f,0x6f						//8,9
};


void TM1637_init(void)
{
	_delay_ms(5);
	TM1637DDR |= (1 << TM1637DATA) | (1 << TM1637CLK);			//DATAPIN en CLKPIN als output
}


void TM1637_WriteByte(uint8_t data)
{
	uint8_t bitcounter = 0;

	_delay_us(5);
	for (bitcounter = 0; bitcounter < 8; bitcounter++)
	{
		_delay_us(5);
		if (data & 0x01) TM1637PORT |= (1 << TM1637DATA);			//set data on DATAPIN
		else TM1637PORT &= ~(1 << TM1637DATA);

		_delay_us(5);
		TM1637PORT |= (1 << TM1637CLK);								//generate clock pulse
		_delay_us(5);
		TM1637PORT &= ~(1 << TM1637CLK);

		data >>= 1;													//next bit
	}
	_delay_us(5);
	_delay_us(5);
	TM1637PORT |= (1 << TM1637CLK);									//generate 9th clock pulse
	_delay_us(5);
	TM1637PORT &= ~(1 << TM1637CLK);
	_delay_us(5);
}


void TM1637_start(void)
{
	TM1637PORT |= (1 << TM1637CLK);										//make clock high
	TM1637PORT |= (1 << TM1637DATA);									//make data high
	TM1637PORT &= ~(1 << TM1637DATA);									//data low
	_delay_us(5);
	TM1637PORT &= ~(1 << TM1637CLK);									//clock low

}


void TM1637_stop(void)
{
	TM1637PORT &= ~(1 << TM1637DATA);									//data low													//wait
	TM1637PORT &= ~(1 << TM1637CLK);									//clock low
	TM1637PORT |= (1 << TM1637CLK);										//make clock high
	_delay_us(5);
	TM1637PORT |= (1 << TM1637DATA);									//make data high
	_delay_us(5);
}



void TM1637_display(uint8_t digit, uint8_t  data, uint8_t digpoint)
{
	uint8_t sevenseg = 0;

	_delay_us(5);
	sevenseg = TM1637_decode(data, digpoint);
	
	TM1637_start();
	TM1637_WriteByte(TM1637_ADDR);
	TM1637_stop();

	TM1637_start();
	TM1637_WriteByte((0xC0 | digit));
	TM1637_WriteByte(sevenseg);
	TM1637_stop();

	//TM1637_start();
	//TM1637_WriteByte((0x88 | 7));										//TODO brightness
	//TM1637_stop();
}


uint8_t TM1637_decode(uint8_t data, uint8_t digpoint)
{
	uint8_t decoded_data = 0;

	if (data == 0x7F) return 0;

	decoded_data = decode_table[data];
	if (digpoint) decoded_data |= 0x80;

	return decoded_data;
}


void TM1637_showdecimal(uint16_t number, uint8_t digpoint)
{
	int8_t i = 0;

	if (number <= 9999)
	{
		for (i = 3; i > -1; i--)
		{
			TM1637_display(i, number%10, digpoint);
			number /= 10;
		}
	}
	else
	{
		TM1637_clear();
	}
}


void TM1637_show_clock(uint8_t hour, uint8_t minute, uint8_t colon)
{
	uint8_t tens = 0;
	uint8_t units = 0;

	units = hour % 10;
	tens = hour / 10;

	TM1637_display(0, tens, colon);
	TM1637_display(1, units, colon);

	units = minute % 10;
	tens = minute / 10;

	TM1637_display(2, tens, colon);
	TM1637_display(3, units, colon);
}


void TM1637_clear(void)
{
	TM1637_display(0, 0x7F, 0);
	TM1637_display(1, 0x7F, 0);
	TM1637_display(2, 0x7F, 0);
	TM1637_display(3, 0x7F, 0);
}


void TM1637_brightness(uint8_t brightness)
{
	if (brightness > 7) brightness = 7;
	TM1637_start();
	TM1637_WriteByte((0x88 | brightness));										//TODO brightness
	TM1637_stop();
}


