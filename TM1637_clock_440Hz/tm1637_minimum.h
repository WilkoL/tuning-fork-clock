#ifndef TM1637_MINIMUM_H
#define TM1637_MINIMUM_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>

//set this to the actual ports
#define TM1637CLK 		PB3	
#define TM1637DATA 		PB4
#define TM1637PORT 		PORTB
#define TM1637DDR		DDRB
#define TM1637INPUT		PINB

#define TM1637_ADDR	0x44

//public functions
void TM1637_init(void);
void TM1637_showdecimal(uint16_t, uint8_t);
void TM1637_show_clock(uint8_t, uint8_t, uint8_t);
void TM1637_clear(void);
void TM1637_brightness(uint8_t);


#endif