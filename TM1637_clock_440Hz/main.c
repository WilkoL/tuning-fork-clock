/*
* TM1637_clock_440Hz.c
* ATTiny13 interne clock 9.6 MHz
*
* TIMER0 met externe clock afkomstig van
* 440 Hz stemvork oscillator
*
* Created: 14-8-2019 14:37:02
* Author : wilko
*/

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "tm1637_minimum.h"

volatile uint8_t semaphore;

volatile uint8_t uur;
volatile uint8_t minuut;
volatile uint8_t seconde;
volatile uint8_t dubbele_punt;

int main(void)
{
	uint8_t helderheid = 1;
	
	uur = 12;
	minuut = 30;
	seconde = 0;
	dubbele_punt = 0;
	
	TCCR0A |= (1 << WGM01) | (1 << WGM00);				//fast pwm, top=OCRA
	TCCR0B |= (1 << WGM02) | (1 << CS02) | (1 << CS01);	//externe clock, falling edge
	TCNT0 = 0;
	OCR0A = 219;										//elke 220 clock pulsen
	TIMSK0 |= (1 << TOIE0);								//interrupt bij overflow
	
	DDRB &= ~(1 << PB0);								//PB0 input button1
	DDRB &= ~(1 << PB1);								//PB0 input button2
	PORTB |= (1 << PB0);								//pullup
	PORTB |= (1 << PB1);								//pullup
	
	DDRB &= ~(1 << PB2);								//PB2 input (T0)
	
	GIFR |= (1 << PCIF);								//clear pin change irq flag
	GIMSK |= (1 << PCIE);								//pin change irq enable
	PCMSK |= (1 << PCINT0) | ( 1 << PCINT1);			//enable pcint0 en 1
	
	sei();
	
	TM1637_init();
	
	while (1)
	{
		if (semaphore == 1)
		{
			semaphore = 0;
			dubbele_punt = seconde % 2;
			
			TM1637_show_clock(uur, minuut, dubbele_punt);
			
			if ((uur >= 0) && (uur <=5))  helderheid = 0;
			if ((uur > 5)  && (uur <=8))  helderheid = 2;
			if ((uur > 8)  && (uur <=18)) helderheid = 7;
			if ((uur > 18) && (uur <=21)) helderheid = 2;
			if ((uur > 21) && (uur <=23)) helderheid = 0;
			TM1637_brightness(helderheid);

		}
		
		if (semaphore == 2)
		{
			semaphore = 0;
			
			seconde = 0;
			if ((PINB & _BV(PB0)) == 0) 
			{
				if (uur < 23) uur++;
				else uur = 0;
			}
			if ((PINB & _BV(PB1)) == 0) 
			{
				if (minuut < 59) minuut++;
				else minuut = 0;
			}
			
			TM1637_show_clock(uur, minuut, dubbele_punt);
			
			_delay_ms(200);								//negeer contact bounce
			PCMSK |= (1 << PCINT0) | ( 1 << PCINT1);	//enable pcint0 en 1
		}
	}
}


ISR(TIM0_OVF_vect)										//2x per seconde
{
	static uint8_t halve_seconde;
	
	PCMSK |= (1 << PCINT0) | ( 1 << PCINT1);			//enable pcint0 en 1
	
	if (halve_seconde++ < 1)
	{
		if (seconde < 59) seconde++;
		else
		{
			seconde = 0;
			if (minuut < 59) minuut++;
			else
			{
				minuut = 0;
				if (uur < 23) uur++;
				else
				{
					uur = 0;
				}
			}
		}
	}
	else
	{
		halve_seconde = 0;
		semaphore = 1;
	}
}


ISR(PCINT0_vect)
{
	PCMSK &= ~(1 << PCINT0);							//disable PCINT0
	PCMSK &= ~(1 << PCINT1);							//disable PCINT1
	semaphore = 2;
}
