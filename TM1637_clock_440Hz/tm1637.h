#ifndef TM1637_H
#define TM1637_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <avr/io.h>

//set this to the actual ports
#define TM1637CLK 		PB3	
#define TM1637DATA 		PB4
#define TM1637PORT 		PORTB
#define TM1637DDR		DDRB
#define TM1637INPUT		PINB

#define TM1637_ADDR	0x44

//#define STARTADDR	0xc0

#define  DARK		0
#define  TYPICAL 	2
#define  BRIGHT  	7

#define _A 		10
#define _B 		11
#define _C 		12
#define _D 		13
#define _E 		14
#define _F 		15
#define _H 		16
#define _I 		17
#define _J 		18
#define _L 		19
#define _O 		20
#define _P 		21
#define _R 		22
#define _S 		23
#define _T 		24
#define _U 		25
#define _Z 		26
#define _minus 	27
#define _underscore 28
#define _degree 	29
#define _space 	30

//public functions
void TM1637_display(uint8_t, uint8_t, uint8_t);
void TM1637_showdecimal(uint16_t, uint8_t);
void TM1637_showdecimal_with_leading_zeros(uint16_t, uint8_t);
void TM1637_showhex(uint16_t);
void TM1637_show_clock(uint8_t, uint8_t, uint8_t);
void TM1637_clear(void);
//void TM1637_brightness(uint8_t);


#endif